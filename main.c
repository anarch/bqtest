/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                    Main                                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "init.c"

/*
** メインプログラム
*/
int main(int argc, char *argv[])
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
  glutInitWindowSize(800, 600);
  glutCreateWindow("gltest");
  mikmodinit("music/inspace.xm");
  glinit();
  glutMainLoop();
  return 0;
}

