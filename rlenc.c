/* bmp ARGB RLE compresion */

#include "rle.c"

int main(int argc, char *argv[]) {
int isize = atoi(argv[1]);
int rlemode = atoi(argv[2]);
if (isize == 0) {
printf("args invaild\r\nrlenc [size] [mode]\r\nsize=2n, mode0=rle, 1=raw\r\n");
return 1;
}
if (rlemode) {
    bmpraw("rlein", "rleout", isize);
} else {
    encrle("rlein", "rleout", isize);
}
return 0;
}