CC            = gcc
CFLAGS        = -O2 -Wall -I/usr/local/include
LDFLAGS       = -L/usr/local/lib
LIBS          = -lfreeglut -lglu32 -lopengl32 -lmikmod
OBJS          = main.o
PROGRAM       = main

all:		$(PROGRAM)

$(PROGRAM):	$(OBJS)
		$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $(PROGRAM)

clean:
		rm -f *.o *~ $(PROGRAM)