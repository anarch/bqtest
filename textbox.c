/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                           Text Related Functions                           */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include "bdf.c"
  
  GLubyte texturekp[26][64][64][4];
  GLubyte *texturefr;
  GLubyte texturenm[16][16][3];
  GLubyte texturesc[16][16][3];
  GLubyte texturecu[16][16][3];
  int entext = 1;
  int enname = 0;
  int textbox[27];
  char names[8][16];
  char stagenamebuffer[8] = {'S', 't', 'a', 'g', 'e', ' ', '1', 0};
  char namestage = 0;
  int curname = 0;
  unsigned short messages[256][26];
  int messagedispnum = 0;
  int messagecomp = 0;
  int messagecomptimer = 0;

void updatename(int num) {
  curname = num;
}

void ennamestage(int en) {
  namestage = en;
}

void changestagenumber(int num) {
  stagenamebuffer[6] = 48 + num;
}

void setentext(int text, int name) {
  enname = name;
  entext = text;
}

static int disppow(int x, int y) {
  int j;
  int k = x;
  for (j = 0; j < y; j++) {
    k = k*x;
  }
  return k;
}

static int dispdigit(int x) {
  int y = 0;
  while (x > 10) {
    x = x / 10;
    y++;
  }
  return y;
}

unsigned char digittochar(int score, int num) {
  unsigned char buf;
  int j;
  int k;
  score = score * 10;
  k = score % disppow(10, num + 1);
  buf = (k / disppow(10, num)) + 48;
  return buf;
}

void displayscore(int score, float x, float y) {
  unsigned char msgscore[9] = "        ";
  int j;
  int k;
  score = score * 10;
  for (j = 0; j < dispdigit(score); j++) {
    k = score % disppow(10, j + 1);
    msgscore[7 - j] = (k / disppow(10, j)) + 48;
  }
  /* display number */
  glDisable(GL_LIGHTING);
  glColor3d(1.0, 1.0, 1.0);
  glRasterPos3d(x, y, 1.0);
  glPushAttrib(GL_CURRENT_BIT);
  for (j = 0; j < 8; j++) {
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msgscore[j]);
  }
  glPopAttrib();
  glFlush();
  glEnable(GL_LIGHTING);
}

void displayscores(int stage, int lives, int score, float x, float y) {
  char msgstage[9] = "Stage: 0";
  char msglives[9] = "Lives: 0";
  char msgscore[16] = "Score:         ";
  int j;
  int k;
  msgstage[7] = stage + 48;
  msglives[7] = lives + 48;
  score = score * 10;
  for (j = 0; j < dispdigit(score); j++) {
    k = score % disppow(10, j + 1);
    msgscore[14 - j] = (k / disppow(10, j)) + 48;
  }
  /* display stage number */
  glDisable(GL_LIGHTING);
  glColor3d(1.0, 1.0, 1.0);
  glRasterPos3d(x, y, 1.0);
  glPushAttrib(GL_CURRENT_BIT);
  for (j = 0; j < 8; j++) {
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msgstage[j]);
  }
  glRasterPos3d(x, y - 0.1, 1.0);
  for (j = 0; j < 8; j++) {
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msglives[j]);
  }
  glRasterPos3d(x, y - 0.2, 1.0);
  for (j = 0; j < 15; j++) {
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msgscore[j]);
  }
  glPopAttrib();
  glFlush();
  glEnable(GL_LIGHTING);

  /* draw frame, same as name for now */
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 16, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, texturenm);
    glEnable(GL_TEXTURE_2D);
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
      glTexCoord2d(0.0, 1.0);
		  glVertex3f(x+0.3, y+0.1, 0.05);
      glTexCoord2d(1.0, 1.0);
		  glVertex3f(x+1.3 , y+0.1, 0.05);
      glTexCoord2d(1.0, 0.0);
		  glVertex3f(x+1.3 , y+0.7, 0.05);
      glTexCoord2d(0.0, 0.0);
      glVertex3f(x+0.3, y+0.7, 0.05);
	  glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);

}

void displayitemsstatus(int timef, int times, int timet, float x, float y) {
  char msgitem[7] = "Item 0";
  char msgtime[3][4] = {"000", "000", "000"};
  int tm[3] = {0, 0, 0};
  int j;
  int k;
  int l;
  if (timef > 10)
    tm[0] = timef * 10;
  if (times > 10)
    tm[1] = times * 10;
  if (timet > 10)
    tm[2] = timet * 10;
  for(l = 0; l < 3; l++) {
    if (tm[l]) {
      for (j = 0; j < 3; j++) {
        k = tm[l] % disppow(10, j + 1);
        msgtime[l][2 - j] = (k / disppow(10, j)) + 48;
      }
    }
  }
  /* display items */
  glDisable(GL_LIGHTING);
  glColor3d(1.0, 1.0, 1.0);
  glPushAttrib(GL_CURRENT_BIT);
  for (l = 0; l < 3; l++) {
    if (tm[l] == 0 && gameitemsgetbullet() == 0) {
      continue;
    }
    msgitem[5] = 49 + l;
    glRasterPos3d(x, y+0.1*l, 1.0);
    for (j = 0; j < 7; j++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msgitem[j]);
    }
    if (timef > 10) {
      glRasterPos3d(x+0.3, y+0.1*l, 1.0);
      for (j = 0; j < 3; j++) {
          glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msgtime[l][j]);
      }
    } else if(gameitemsgetbullet() > 0) {
      glRasterPos3d(x+0.3, y+0.1*l, 1.0);
      msgtime[l][2] = 48 + gameitemsgetbullet();
      for (j = 0; j < 3; j++) {
          glutBitmapCharacter(GLUT_BITMAP_9_BY_15, msgtime[l][j]);
      }
      break;
    }
  }

  glPopAttrib();
  glFlush();
  glEnable(GL_LIGHTING);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 16, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, texturenm);
    glEnable(GL_TEXTURE_2D);
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
      glTexCoord2d(0.0, 1.0);
		  glVertex3f(x+0.3, y-0.3, 0.05);
      glTexCoord2d(1.0, 1.0);
		  glVertex3f(x+1.3 , y-0.3, 0.05);
      glTexCoord2d(1.0, 0.0);
		  glVertex3f(x+1.3 , y+0.3, 0.05);
      glTexCoord2d(0.0, 0.0);
      glVertex3f(x+0.3, y+0.3, 0.05);
	  glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

static void displayname(char* message){
  if (enname == 1) {
    glDisable(GL_LIGHTING);
    glColor3d(1.0, 1.0, 1.0);
    glRasterPos3d(-1.5, -1.0, 1.0);
    glPushAttrib(GL_CURRENT_BIT);
	  while (*message)
	    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *message++);
    glPopAttrib();
    glFlush();
    glEnable(GL_LIGHTING);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 16, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, texturenm);
    glEnable(GL_TEXTURE_2D);
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS); /* draw name frame */
      glTexCoord2d(0.0, 1.0);
		  glVertex3f(-2.25, -1.5, 0.05);
      glTexCoord2d(1.0, 1.0);
		  glVertex3f(-1.0 , -1.5, 0.05);
      glTexCoord2d(1.0, 0.0);
		  glVertex3f(-1.0 , -1.4, 0.05);
      glTexCoord2d(0.0, 0.0);
      glVertex3f(-2.25, -1.4, 0.05);
	  glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D); /* Only use this if not using shaders. */
  }
}

void displaynameupdate() {
  if (namestage == 0) {
    displayname(names[curname]);
  } else {
    displayname(stagenamebuffer);
  }
}

  void loadframe() {
    FILE *fp;
    unsigned char *tex;

    tex = loadrle("rle/textfrm");
    texturefr = (GLubyte *)malloc(16*16*4);
    memcpy(texturefr, tex, 16*16*4);
    free(tex);
    if ((fp = fopen("rle/namefrm", "rb")) != NULL) {
      fread(texturenm, sizeof texturenm, 1, fp);
      fclose(fp);
    }
    if ((fp = fopen("rle/scorefrm", "rb")) != NULL) {
      fread(texturesc, sizeof texturesc, 1, fp);
      fclose(fp);
    }
    if ((fp = fopen("rle/select", "rb")) != NULL) {
      fread(texturecu, sizeof texturecu, 1, fp);
      fclose(fp);
    }
  }

int fontblit(short encnum, int place) {
  int i;
  int j;
  int encoding = 6880;
  if (encnum == 0) {
    return 0;
  }
  for (j = 0; j < 6879; j++) {
    if (fontnum[j] == encnum) {
      encoding = j;
      break;
    }
  }
  if (encoding == 6880) {
    return 0;
  }
    for (i = 0; i < 64; i++) {
        for (j = 0; j < 64; j++) {
          if (j < 23 && i < 20) {
            if (fontbuf[encoding][i] & (1<<j)) {
                texturekp[place][i][64-j][0] = 255-i*10;
                texturekp[place][i][64-j][1] = 0;
                texturekp[place][i][64-j][2] = 0;
                texturekp[place][i][64-j][3] = 255;
            } else {
                texturekp[place][i][64-j][0] = 255;
                texturekp[place][i][64-j][1] = 255;
                texturekp[place][i][64-j][2] = 255;
                texturekp[place][i][64-j][3] = 0;
            }
          } else {
              texturekp[place][i][64-j][0] = 0;
              texturekp[place][i][64-j][1] = 0;
              texturekp[place][i][64-j][2] = 0;
              texturekp[place][i][64-j][3] = 0;
          }
        }
    }
    return 0;
}

void loadmessage(char* fname) {
  int j;
  int k;
  FILE *fp;
  short buffer;
  short bs;
    if ((fp = fopen(fname, "rb")) != NULL) {
    } else {
      printf("no messages found");
      exit(0);
    }
  for (j = 0; j < 256; j++) { 
    /* load until line 256 or eof */
    fseek(fp, 3, SEEK_CUR);
    for (k = 0; k < 27; k++) {
      if (feof(fp)) {
        goto loadmsgout;
      }
      fread(&buffer, sizeof(short), 1, fp);
      if (buffer == 0x281B) {
        fseek(fp, 3, SEEK_CUR);
        break;
      } else {
        bs = buffer & 0xFF;
        bs = bs << 8;
        buffer = buffer >> 8;
        buffer = buffer + bs;
        messages[j][k] = buffer;
      }
    }
  }
loadmsgout:
  fclose(fp);
}

void displaycursor(float x, float y) {
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 16, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, texturecu);
  glEnable(GL_TEXTURE_2D);
  glNormal3d(0.0, 0.0, 1.0);
	glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
		glVertex3f(x, y, 0.05);
    glTexCoord2d(1.0, 0.0);
		glVertex3f(x+0.25 , y, 0.05);
    glTexCoord2d(1.0, 1.0);
		glVertex3f(x+0.25 , y+0.25, 0.05);
    glTexCoord2d(0.0, 1.0);
    glVertex3f(x, y+0.25, 0.05);
	glEnd();
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
}

short gettitlemessage(short stage, short num) {
  short buf = messages[14+stage][num];
  return buf;
}

void displaymessageupdate(int speed) {
  if (messagecomp > 25) {
  return;  
  }
  messagecomptimer = messagecomptimer - speed;
  if (messagecomptimer < 1 && entext == 1) {
    if (messages[messagedispnum][messagecomp] != 0) {
      playstext();
    }
    fontblit(messages[messagedispnum][messagecomp], messagecomp);
    messagecomptimer = 8;
    messagecomp++;
  }
}

void displaymessageclear() {
  int j;
  for (j = 0; j < 26; j++) {
    fontblit(8481, j);
  }
}

void displaymessageset(int num) {
  messagedispnum = num;
  messagecomp = 0;
  displaymessageclear();
  
}

void chardraw(float x, float y, int num) {
  static const GLfloat color[] = { 1.0, 1.0, 1.0, 1.0 };  /* 材質 (色) */
  
  /* 材質の設定 */
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
  glEnable(GL_ALPHA_TEST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, texturekp[num]);
  glEnable(GL_TEXTURE_2D);

  glNormal3d(0.0, 0.0, 1.0);
  /* glTranslatef(x, y, 0.1); */
  glBegin(GL_QUADS);
  glTexCoord2d(0.0, 1.0);
  glVertex3d(-0.8+x, -1.5+y,  0.3);
  glTexCoord2d(1.0, 1.0);
  glVertex3d(0.2+x, -1.5+y,  0.3);
  glTexCoord2d(1.0, 0.0);
  glVertex3d(0.2+x,  -0.5+y,  0.3);
  glTexCoord2d(0.0, 0.0);
  glVertex3d(-0.8+x,  -0.5+y,  0.3);
  glEnd();

  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_ALPHA_TEST); 

}

void textdraw() {
  float curline = 0;
  float y = 0;
  int j;
  if (entext == 1) {
  static const GLfloat color[] = { 1.0, 1.0, 1.0, 1.0 };  /* 材質 (色) */
  
  /* 材質の設定 */
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
  
  /* アルファテスト開始 */
  glEnable(GL_ALPHA_TEST);
  /* glEnable(GL_BLEND); */

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 16, 16, 0, GL_RGBA, GL_UNSIGNED_BYTE, texturefr);
  glEnable(GL_TEXTURE_2D);
  glNormal3d(0.0, 0.0, 1.0);
	glBegin(GL_QUADS); /* draw frame */
    glTexCoord2d(0.0, 1.0);
		glVertex3f(-2.2, -1.2, 0.05);
    glTexCoord2d(1.0, 1.0);
		glVertex3f(2.2 , -1.2, 0.05);
    glTexCoord2d(1.0, 0.0);
		glVertex3f(2.2 , -0.45, 0.05);
    glTexCoord2d(0.0, 0.0);
    glVertex3f(-2.2, -0.45, 0.05);
	glEnd();
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D); /* Only use this if not using shaders. */

  /* glDisable(GL_BLEND); */

  glEnable(GL_ALPHA_TEST); 
      
  for (j = 0; j < 26; j++) {
    if (j < 13) {
      curline = 0;
      y = j / 3.0 -2;
    } else {
      curline = -0.35;
      y = j / 3.0 -6.33;
    }

  /* 44x44, 70 to 570, 350 to 400, 13x2=26char */
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, texturekp[j]);
  glEnable(GL_TEXTURE_2D);

  glNormal3d(0.0, 0.0, 1.0);
  glBegin(GL_QUADS);
  glTexCoord2d(0.0, 1.0);
  glVertex3d(y-0.8, curline-1.5,  0.1);
  glTexCoord2d(1.0, 1.0);
  glVertex3d( y+0.2, curline-1.5,  0.1);
  glTexCoord2d(1.0, 0.0);
  glVertex3d( y+0.2,  curline-0.5,  0.1);
  glTexCoord2d(0.0, 0.0);
  glVertex3d(y-0.8,  curline-0.5,  0.1);
  glEnd();

  /* Cleanup texture binds. */
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D); /* Only use this if not using shaders. */
  }
  glDisable(GL_ALPHA_TEST);
      }
  }