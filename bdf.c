/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                 BDF Loader                                 */
/*                                for k20m.bdf                                */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

/* global variables */
long fontbuf[6879][20]; /* bitmap and lines, 6878 chars, 1st blank */
int fontnum[6879]; /* encoding numbers */

int loadfont(char *filename) { /* ret: font array */
    char buf; /* search buffer */
    char rowbuf[7]; /* font row buffer */
    char encbuf[6]; /* encoding number buffer */
    int bitflag = 0;
    int encflag = 0;
    int bc = 0;
    int encnum = 0;
    int i;
    int j;
    int k;
    FILE *fp;
    /*
    int **fontbuf = calloc(6878, sizeof(int *));
    for (int y = 0; y < 6878; y += 1) {
       fontbuf[y] = calloc(20, sizeof(int));
    }
    */
    fp = fopen(filename, "rb");
    fseek(fp, 1216, SEEK_SET); /* skip k20m bdf header */
    /* printf("B is %c, %i\n", "B", "B"); */
    for (i = 0; i < 439835; i++) { /* k20m size ... */
        fread(&buf, sizeof(char), 1, fp);
        /* printf("buf is %c, %i\n", buf, buf); */
        /* encoding load */
        if (buf == 69) { /* search "ENC" */
            /* printf("hit E\n"); */
            fread(&buf, sizeof(char), 1, fp);
            if (buf == 78) {
                /* printf("hit N\n"); */
                fread(&buf, sizeof(char), 1, fp);
                if (buf == 67) {
                    /* printf("hit C, encflag = 1\n"); */
                    encflag = 1;
                }
            }
        }
        /* bitmap load */
        if (buf == 66) { /* search "BI" */
            /* printf("hit B\n"); */
            fread(&buf, sizeof(char), 1, fp);
            if (buf == 73) {
                /* printf("hit I, bitflag = 1\n"); */
                bitflag = 1;
            }
        }
        if (encflag == 1) {
            fseek(fp, 6, SEEK_CUR); /* skip "ODING\n" */
            for(k = 0; k < 6; k++) {
                fread(&encbuf[k], sizeof(char), 1, fp);
                if (encbuf[k] == 10) {
                    encbuf[k] == 0;
                }
            }
            /* printf("it's %ith encode, %i\n", encnum, atoi(encbuf)); */
            fontnum[encnum] = atoi(encbuf);
            encflag = 0;
            encnum++;
        }
        if (bitflag == 1) {
            /* printf("it's %i bitmap\n", bc); */
            fseek(fp, 5, SEEK_CUR); /* skip "TMAP\n" */
            for (j = 0; j < 20; j++) {
                fread(&rowbuf, sizeof(char), 6, fp);
                fseek(fp, 1, SEEK_CUR); /* skip \n */
                rowbuf[6] = 0;
                fontbuf[bc][j] = strtol(rowbuf, NULL, 16);
            }
            bc++;
            bitflag = 0;
        }
    }
    printf("%ld\n", fontbuf[6878][0]);
    return 0;
}



/* int main(int argc, char *argv[])
{
    loadfont("k20m.bdf");
    printf("%ld\n", fontbuf[6800][0]);
    printf("%ld\n", fontnum[6800]);
    return 0;
} */