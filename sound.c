/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                 Sound Init                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <mikmod.h>

    MODULE *module;
    SAMPLE *sshot;
    SAMPLE *stext;

    char mvolume = 60;

int mikmodinit(char *fname) {

    /* initialize MikMod threads */
	MikMod_InitThreads ();

	/* register all the drivers */
	MikMod_RegisterAllDrivers();

	/* register all the module loaders */
	MikMod_RegisterAllLoaders();

	/* init the library */
	md_mode |= DMODE_STEREO | DMODE_SOFT_MUSIC | DMODE_SOFT_SNDFX | DMODE_INTERP;
	if (MikMod_Init("")) {
		fprintf(stderr, "Could not initialize sound, reason: %s\n",
				MikMod_strerror(MikMod_errno));
		return 2;
	}
    
    /* load module */
	module = Player_Load(fname, 64, 0);
    sshot = Sample_Load("sound/shot.wav");
    sshot->volume = 32;
    stext = Sample_Load("sound/text.wav");
    stext->volume = 8;
    /* sshot->flags = SF_LOOP;
    sshot->loopstart = 0;
    sshot->loopend = 1070; */
    if (!sshot) {
        MikMod_Exit();
        fprintf(stderr, "Could not load the first sound, reason: %s\n",
        MikMod_strerror(MikMod_errno));
        return 2;
    }
    MikMod_SetNumVoices(-1, 2);
    MikMod_EnableOutput();

    if (module) {
		/* start module */
    /* Player_SetVolume(mvolume); */
    } else {
		fprintf(stderr, "Could not load module, reason: %s\n", MikMod_strerror(MikMod_errno));
    }

    return 0;
}

void mikmodreset(int s) {
  if (!s) {
    md_mode |= DMODE_STEREO;
  }  else {
    md_mode &= ~DMODE_STEREO;
  }
  MikMod_Reset("");
}

void loadmodule(char *fname, int loop) {
    module = Player_Load(fname, 64, 0);
    if (module) {
		/* start module */
    if (loop != 0) {
      module->wrap = 1;
    }
		printf("Playing %s (%d chn)\n", module->songname, (int) module->numchn);
		Player_Start(module);
    } else {
		fprintf(stderr, "Could not load module, reason: %s\n", MikMod_strerror(MikMod_errno));
    }
}

void setvolume(char v) {
  mvolume = v;
  Player_SetVolume(mvolume);
}

void stopsound() {
    	Player_Stop();
      Player_Free(module);
}