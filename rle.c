/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                     RLE                                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

unsigned long rledec(void* dest, void* src, long width, long height) {

    if (dest == NULL || src == NULL || width <= 0 || height <= 0)
    {
        return 0;
    }
 
    const unsigned char* psrc = (const unsigned char*)src;
	unsigned char* pdest = (unsigned char*)dest;
 
    long len = (width * 8 + 31) / 32 * 4;
    long size = len * height;

    long x;
    long i;
 
    memset(dest, 0, size);

	int eob = 0;
        for (x = 0; ; )
        {
            unsigned char code[2];
            code[0] = *psrc++;
            code[1] = *psrc++;
 
            if (code[0] == 0)
            {
                int eol = 0;

                switch (code[1])
                {
                case 0x00:
                    eol = 1;
                    break;
 
                case 0x01:
                    eob = 1;
                    break;
 
                case 0x02:
                    code[0] = *psrc++;
                    code[1] = *psrc++;
                    x += code[0];
                    pdest += code[0] + len * code[1];
                    break;
 
                default:
                    x += code[1];
                    for (i = 0; i < (long)code[1]; i++)
                    {
                        *pdest++ = *psrc++;
                    }
 
                    if ((code[1] & 1) != 0) { psrc++; }
 
                    break;
                }
 
                if (eol || eob) { break; }
            }
            else
            {
                x += code[0];
                for (i = 0; i < (long)code[0]; i++)
                {
                    *pdest++ = code[1];
                }
            }
        }
    return size;
}

unsigned long srle(unsigned char* dest, unsigned char data, long datanum, int bits) {
	unsigned char* pdest = (unsigned char*)dest;
	if (datanum >= 1)
    	{
        	*pdest++ = (unsigned char)(datanum * 8 / bits);
        	*pdest++ = data;
    	}
 
    return (unsigned long)(pdest - dest);
}

unsigned long sabs(unsigned char* dest, const void* src, long srcsize, int bits) {
    long i;
	unsigned char* pdest = (unsigned char*)dest;
    	unsigned char* psrc = (unsigned char*)src;
 
    	if (srcsize >= 3){
        	*pdest++ = 0x00;
        	*pdest++ = (unsigned char)(srcsize * 8 / bits);
        	for (i = 0; i < srcsize; i++) {
            		*pdest++ = *(psrc + i);
        	}
        if ((srcsize & 1) != 0) { *pdest++ = 0x00; }
    }
    else {
        for (i = 0; i < srcsize; i++) {
            pdest += srle(pdest, *(psrc + i), 1, bits);
        }
    }
 
    return (unsigned long)(pdest - dest);
}

unsigned char *encrle(char *filename, char *outname,  int size) {
    FILE *fp;
    int j;
    int k;
    int i;
  

    unsigned char (*data)[size][4] = (unsigned char(*)[size][4])malloc(size*size*4*sizeof(unsigned char));
    unsigned char (*conv)[size][size] = (unsigned char(*)[size][size])malloc(size*size*4*sizeof(unsigned char));
    unsigned char (*conp)[size][size] = (unsigned char(*)[size][size])malloc(size*size*4*sizeof(unsigned char));
    int file;
    file = sizeof(data[0][0][0]);
    int hskip;

    fp = fopen(filename, "rb");
    if (fp == NULL) {
                printf("File cant open\n");
                return 0;
        }
    fseek(fp, 10, SEEK_SET);
    fread(&hskip, sizeof(int), 1, fp);
    fseek(fp, hskip, SEEK_SET);
    
    file = fread(data, sizeof(data[0][0][0]), size*size*4, fp );
    if(file < sizeof(data)) {
        	fputs( "file load fail\n", stderr );
        	return 0;
    	}
    fclose(fp);
    
    for(k = 0; k < size; k++) {
        	for(j = 0; j < size; j++) {
			for(i = 0; i < 4; i++) {
				conv[i][k][j] = data[k][j][i];
			}
		}
	}
    

    /*
    fp = fopen(outname, "wb");
    for(int k = 0; k < 4; k++) {
        fwrite(conv[k], sizeof(char), sizeof(conv[k]), fp);
    }
    fclose(fp);
    */

    long len[4];

        unsigned char* pdest;
        unsigned char* pdestc;
        unsigned char* psrc;
        unsigned char* pabs;
        unsigned char prev;

    for (k = 0; k < 4; k++) {
        long count = 1;
        long abs = -1;
        psrc = conv[k];
        prev = (*psrc == 0) ? 1 : 0;
        pdest = conp[k];
        pdestc = conp[k];
        pabs = psrc;
        for(i = 0; i < size*size; i++) {
            unsigned char cur = *(psrc + i);
            if (cur == prev) {
                count++;
                if (abs >= 1) {
                    pdest += sabs(pdest, pabs, abs, 8);
                } else if (count == 255) {
                    pdest += srle(pdest, prev, 255, 8);
                    count = 1;
                    prev = (*(psrc + i + 1) == 0) ? 1 : 0;
                }
                abs = -1;
            } else {
                abs++;
                if (count >= 2) {
                    pdest += srle(pdest, prev, count, 8);
                } else if (abs == 255) {
                    pdest += sabs(pdest, pabs, 255, 8);
                    abs = 0;
                }
                count = 1;
                if (abs == 0) {
                    pabs = psrc + i;
                }
                prev = cur;
            }
        }
        if (abs >= 0) {
            pdest += sabs(pdest, pabs, abs + 1, 8);
        }
        else if (count >= 2) {
            pdest += srle(pdest, prev, count, 8);
        }
        len[k] = pdest - pdestc;
    }
    fp = fopen(outname, "wb");
    fwrite("RL_\0", sizeof(char), 4, fp);
    fwrite(&size, 2, 1, fp);
    fwrite(&size, 2, 1, fp);
    fwrite(&len, sizeof(int), 4, fp);
    for(k = 0; k < 4; k++) {
        fwrite(conp[k], sizeof(char), len[k], fp); /* out is argb */
    }
    fclose(fp);
}

void bmpraw(char *filename, char *outname, int size) {
    FILE *fp;
    int hskip;
    unsigned char *data = malloc(size*size*3*sizeof(unsigned char));
    int file;

    fp = fopen(filename, "rb");
    if (fp == NULL) {
                printf("File cant open\n");
                return;
        }
    fseek(fp, 10, SEEK_SET);
    fread(&hskip, sizeof(int), 1, fp);
    fseek(fp, hskip, SEEK_SET);
    
    file = fread(data, sizeof(unsigned char), size*size*3, fp );
    if(file < sizeof(data)) {
        	fputs( "file load fail\n", stderr );
        	return;
    	}
    fclose(fp);
        fp = fopen(outname, "wb");
        fwrite(data, sizeof(char), size*size*3, fp);
        fclose(fp);
        return;
}

unsigned char *loadrle(char *filename) {
    short int wid = 0;
    short int hei = 0;
    long psize[4]; /* rle size argb */
    int i;
	FILE *fp;
	fp = fopen(filename, "rb");
	if (fp == NULL) {
        printf("open fail");
        return 0;
    }

    /* read info */
    fseek(fp, 4, SEEK_SET);
    fread(&wid, sizeof(char), 2, fp);
    fread(&hei, sizeof(char), 2, fp);
    if (wid == 0 || hei  == 0) {
        printf("not a image file");
        return 0;
    }
    for (i = 0; i < 4; i++) {
        fread(&psize[i], sizeof(char), 4, fp);
        if (psize[i] == 0) {
            printf("ARGB size broken");
            return 0;
        }
    }

    unsigned char *asrc, *rsrc, *gsrc, *bsrc;
    asrc = (unsigned char *)calloc(wid*hei,sizeof(unsigned char));
    rsrc = (unsigned char *)calloc(wid*hei,sizeof(unsigned char));
    gsrc = (unsigned char *)calloc(wid*hei,sizeof(unsigned char));
    bsrc = (unsigned char *)calloc(wid*hei,sizeof(unsigned char));
    /*
    char asrc[wid*hei];
    char rsrc[wid*hei];
    char gsrc[wid*hei];
    char bsrc[wid*hei];
    */
    if(fread(asrc, sizeof(char), psize[0], fp) < psize[0]) {
        fputs( "load fail a", stderr );
        return 0;
    }
    if(fread(rsrc, sizeof(char), psize[1], fp) < psize[1]) {
        fputs( "load fail r", stderr );
        return 0;
    }
    if(fread(gsrc, sizeof(char), psize[2], fp) < psize[2]) {
        fputs( "load fail g", stderr );
        return 0;
    }
    if(fread(bsrc, sizeof(char), psize[3], fp) < psize[3]) {
        fputs( "load fail b", stderr );
        return 0;
    }
    fclose(fp);

    unsigned char *buf;
    unsigned char *img = NULL;
    buf = (unsigned char*)malloc(wid*hei);
    img = (unsigned char*)malloc(wid*hei*4);
    rledec(buf, asrc, wid, hei);
    for(i = 0;i < wid*hei; i++) {
        img[i*4+2] = buf[i];
    }
    free(buf);
    buf = malloc(wid*hei);
    rledec(buf, rsrc, wid, hei);
    for(i = 0;i < wid*hei; i++) {
        img[i*4+1] = buf[i];
    }
    free(buf);
    buf = malloc(wid*hei);
    rledec(buf, gsrc, wid, hei);
    for(i = 0;i < wid*hei; i++) {
        img[i*4] = buf[i];
    }
    free(buf);
    buf = malloc(wid*hei);
    rledec(buf, bsrc, wid, hei);
    for(i = 0;i < wid*hei; i++) {
        img[i*4+3] = buf[i];
    }
    free(buf);
    free(asrc);
    free(rsrc);
    free(gsrc);
    free(bsrc);
    return img; /* out rgba */
}