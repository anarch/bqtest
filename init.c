/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                    Init                                    */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>
#include "sound.c"
#include "rle.c"
#include "textbox.c"
#include "scene/title.c"
#include "scene/game.c"
#include "scene/cscene.c"
#include "scene/ending.c"
#include "scene/option.c"
#include "scene/score.c"
#include "scene/logo.c"

/*
** 光源
*/
static const GLfloat lightpos[] = { 0.0, 0.0, 1.0, 0.0 };
static const GLfloat lightcol[] = { 1.0, 1.0, 1.0, 1.0 }; 
static const GLfloat lightamb[] = { 0.1, 0.1, 0.1, 1.0 };

/*
** テクスチャ
*/
#define TEXWIDTH  256
#define TEXHEIGHT 256


GLdouble s = 0.2;

int j = 0;
float n = 0;

float fpstime;
float basetime;
char fpsstr[64];
int frame = 0;
float fps;
float fpswait = 20.0;

int texloaderr = 0;
float counter1 = 0;
int mikcount = 0;
float eposx = 0;
float eposy = -1.4;
int pressr = 0;
int pressl = 0;
int pressn = 0;
int v1;
int pressj = 0;
float lastshot = 0;

int gameseq = 6;

int fpscolor = 1;

float bullet[7][2];

int fademode = 0;
int fadeint = 0;

int textspeed = 3;

int mmload = 0;


#define FRAMES 360

  /*GLubyte texture[TEXHEIGHT][TEXWIDTH][4];
  GLubyte texturett[TEXHEIGHT][TEXWIDTH][3];
  GLubyte texturesl[32][32][3];
  GLubyte texturebg[TEXHEIGHT][TEXWIDTH][3];*/
  GLubyte texturejk[64][64][3];
  GLubyte texturebl[64][64][3];

/*
** 初期化
*/
static void init(void)
{
  /* テクスチャの読み込みに使う配列 */
static const char texture1[] = ""; /* テクスチャファイル名 */
/*static const char texture1tt[] = "rle/title"; 
static const char texture1sl[] = "rle/select"; 
static const char texture1bg[] = "rle/bgro";
static const char texture1jk[] = "rle/jiki"; 
static const char texture1bl[] = "rle/bullet"; */


  FILE *fp;
  /* log variables */
  /* FILE *logfp; */
  char logb[64];
  char logbool[2][4];

  /* logfp = fopen("main.log", "w");
  fwrite("start ...\n", sizeof(char), 10, logfp); */
  printf("start ...\n");
  sprintf(logb, "keyboard = %i, mouse = %i, %i\n",
  glutDeviceGet(GLUT_HAS_KEYBOARD),
  glutDeviceGet(GLUT_HAS_MOUSE),
  glutDeviceGet(GLUT_NUM_MOUSE_BUTTONS));
  /* fwrite(logb, sizeof(char), 27, logfp); */
  printf(logb);
  sprintf(logb, "MikMod Sound Library version %ld.%ld.%ld\n",
  LIBMIKMOD_VERSION_MAJOR,
  LIBMIKMOD_VERSION_MINOR,
  LIBMIKMOD_REVISION);
  /* fwrite(logb, sizeof(char), 36, logfp); */
  printf(logb);
  /* フォントの読み込み */
  /* fwrite("Loading kappa 20dot fonts ...\n", sizeof(char), 30, logfp); */
  printf("Loading kappa 20dot fonts ...\n");
  loadfont("k20m.bdf");
  sprintf(logb, "font 6800 is %ld, excepted 29494\n", fontnum[6800]);
  /* fwrite(logb, sizeof(char), 35, logfp); */
  printf(logb);
  sprintf(logb, "data 6800 is %ld, excepted 3277056\n", fontbuf[6800][0]);
  /* sfwrite(logb, sizeof(char), 39, logfp); */
  printf(logb);
  if (fontnum[6800] == 29494 && fontbuf[6800][0] == 3277056) {
    printf("correct\n");
  } else {
    printf("incorrect\n");
  }
  
  /* fwrite("Loading images ...\n", sizeof(char), 19, logfp);
  fclose(logfp); */
  
  /* compressed image load... */

  loadmessage("messages.txt");
  strcpy(names[0], "Kusoza Kone"); /* undefined, blue-hair, cardigan, st1 */
  strcpy(names[1], "Savage-chan"); /* speaker on head, cyber themed, st2 */
  strcpy(names[2], "Ita Rosenworcel"); /* blonde, st4 */
  strcpy(names[3], "Vlsi Ramips"); /* lower body is crab, st5 */
  strcpy(names[4], "");
  strcpy(names[5], "");
  strcpy(names[6], "Anarch of ZNM"); 
  strcpy(names[7], "in early 20s"); 
  /*unsigned char *tex = loadrle("rle/rleout");
  memcpy(texture, tex, 256*256*4);
  free(tex); */

  /*
  if ((fp = fopen(texture1tt, "rb")) != NULL) {
    fread(texturett, sizeof texturett, 1, fp);
    fclose(fp);
  } else {texloaderr = 2;}
    if ((fp = fopen(texture1sl, "rb")) != NULL) {
    fread(texturesl, sizeof texturesl, 1, fp);
    fclose(fp);
  } else {texloaderr = 3;}
    if ((fp = fopen(texture1bg, "rb")) != NULL) {
    fread(texturebg, sizeof texturebg, 1, fp);
    fclose(fp);
  } else {texloaderr = 4;}
    if ((fp = fopen(texture1jk, "rb")) != NULL) {
    fread(texturejk, sizeof texturejk, 1, fp);
    fclose(fp);
  } else {texloaderr = 5;}
    if ((fp = fopen(texture1bl, "rb")) != NULL) {
    fread(texturebl, sizeof texturebl, 1, fp);
    fclose(fp);
  } else {texloaderr = 6;}
  */

  loadframe();

  loadgamemaps();

  if (texloaderr > 0) {
    printf("Texture file not found!");
    exit(-1);
  }
  

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  

  /*glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TEXWIDTH, TEXHEIGHT, 0,
               GL_RGBA, GL_UNSIGNED_BYTE, texture); */
  

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  

  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  
#if 0

  static const GLfloat blend[] = { 0.0, 1.0, 0.0, 1.0 };
  glTexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, blend);
#endif
  

  glAlphaFunc(GL_GREATER, 0.5);
  

  glClearColor(0.3, 0.3, 1.0, 0.0);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
  

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightcol);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lightcol);
  glLightfv(GL_LIGHT0, GL_AMBIENT, lightamb);
  glutIgnoreKeyRepeat(GL_TRUE);

  /*for (j = 0;j < 7;j++) {
    bullet[j][0] = 2;
    bullet[j][1] = 2;
  }
  j = 0;*/
}

/*
** シーンの描画
*/
static void scene(void)
{
  static const GLfloat color[] = { 1.0, 1.0, 1.0, 1.0 };  /* 材質 (色) */
  

  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
  

  glEnable(GL_ALPHA_TEST);
  

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TEXWIDTH, TEXHEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, txtitlebg);
  glEnable(GL_TEXTURE_2D);
  

  glNormal3d(0.0, 0.0, 1.0);
  glBegin(GL_QUADS);
  glTexCoord2d(0.0, 1.0);
  glVertex3d(-1.0, -1.0,  0.0);
  glTexCoord2d(1.0, 1.0);
  glVertex3d( 1.0, -1.0,  0.0);
  glTexCoord2d(1.0, 0.0);
  glVertex3d( 1.0,  1.0,  0.0);
  glTexCoord2d(0.0, 0.0);
  glVertex3d(-1.0,  1.0,  0.0);
  glEnd();


  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D); 

  textdraw();

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, texturejk);
  glEnable(GL_TEXTURE_2D); 
  
  glBegin(GL_QUADS);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3d(eposx-0.25, eposy-0.25,  0.0);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3d(eposx+0.25, eposy-0.25,  0.0);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3d(eposx+0.25, eposy+0.25,  0.0);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3d(eposx-0.25,  eposy+0.25,  0.0);
  glEnd();
  


  glDisable(GL_TEXTURE_2D); 

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, texturebl);
  glEnable(GL_TEXTURE_2D);

  j = 0;
  while (j < 7) {
  glBegin(GL_QUADS);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3d(bullet[j][0]-0.05, bullet[j][1]-0.05,  0.0);
  glTexCoord2f(1.0f, 0.0f);
  glVertex3d(bullet[j][0]+0.05, bullet[j][1]-0.05,  0.0);
  glTexCoord2f(1.0f, 1.0f);
  glVertex3d(bullet[j][0]+0.05, bullet[j][1]+0.05,  0.0);
  glTexCoord2f(0.0f, 1.0f);
  glVertex3d(bullet[j][0]-0.05,  bullet[j][1]+0.05,  0.0);
  glEnd();
  j++;
  }
  j = 0;


  glDisable(GL_TEXTURE_2D);
  

  glDisable(GL_ALPHA_TEST);
}

void pbshot() {
  if (counter1 - lastshot < 0.1) {
    return;
  }
  lastshot = counter1;
     while(j < 7) {
     if (bullet[j][0] == 2) {
       break;
     }
     j++;
    }
  bullet[j][0] = eposx;
  bullet[j][1] = eposy;
  j = 0;
  Sample_Play(sshot, 0, SFX_CRITICAL);
}

void gametick() {

  if (pressj == 1) {
    pbshot();
  }
    if (pressl == 1 && eposx > -2) {
        eposx -= 0.05;
        fontblit(eposx*100+1500, 25);
    }
    if (pressr == 1 && eposx < 2) {
        eposx += 0.05;
        fontblit(eposx*100+1500, 25);
    }
    while(j < 7) {
    if (bullet[j][1] < 1.75) {
      bullet[j][1] += 0.05;
    } else {
      bullet[j][0] = 2;
      bullet[j][1] = 2;
    }
    j++;
    }
    j = 0;
}

void changefpscolor(int bw) {
  fpscolor = bw;
}

static void displayText(char* message){
  glDisable(GL_LIGHTING);
  if (fpscolor == 1) {
    glColor3d(1.0, 1.0, 1.0);
  } else {
    glColor3d(0.0, 0.0, 0.0);
  }
  glRasterPos3d(-1.5, 1.0, 1.0);
  glPushAttrib(GL_CURRENT_BIT);
	while (*message)
	  glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *message++);
  glPopAttrib();
  glFlush();
  glEnable(GL_LIGHTING);
}

static void display(void)
{
  static int frame = 0;
  double t = (double)frame / (double)FRAMES;
  
  if (++frame >= FRAMES) frame = 0;
  
  /* テクスチャ行列の設定
  glMatrixMode(GL_TEXTURE);
  glLoadIdentity();
  glTranslated(0.5, 0.5, 0.0);
  glRotated(t * 360.0, 0.0, 0.0, 1.0);
  glTranslated(-0.5, -0.5, 0.0);
  */

  /* zはプラス方向が前 
            ^ y+
            |
            |
      x-    |    x+
      <-----+----->
            |
            |
            | y-
  */
  

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  

  glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
  

  glTranslated(0.0, 0.0, -3.0);
  

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  /*
  gametick();
  */

  /* FPS表示 */
  frame++;
  fpstime = glutGet(GLUT_ELAPSED_TIME);
  if (fpstime - basetime > 10) {
  fps = frame*1000.0/(fpstime-basetime);
  if (!optionfps)
    sprintf(fpsstr, "FPS: %4.2f, Wait: %4.2f", fps, fpswait);
  if (fps > 65) {
    fpswait += 0.1;
  }
  if (fps < 60) {
    if (fpswait > 0)
    fpswait -= 0.1;
  }
  }
  if (!optionfps)
    displayText(fpsstr);
  basetime = fpstime;
  frame = 0;

  /* シーンの描画 */
  /*
  scene();

   game sequence
   title -> game -> score
         -> option, gallery
  */
  if (gameseq < 1) {
    titlescene(); /* to title.c */
  } else if (gameseq < 2) {
    gamescene(); /* to game.c */
  } else if (gameseq < 3) {
    cutscene(); /* to cutscene */
  } else if (gameseq < 4) {
    escene(); /* to ending */
  } else if (gameseq < 5) {
    optionscene(); /* to option */
  } else if (gameseq < 6) {
    scorescene(); /* to score */
  } else if (gameseq < 7) {
    logoscene();
  }

  textdraw();
  displaynameupdate();
  displaymessageupdate(textspeed);
  
  /* ダブルバッファリング */
  glutSwapBuffers();

  /* FPS調整 */
  Sleep(fpswait);
}

void playstext() {
  Sample_Play(stext, 0, 0);
}

void playsshot() {
  Sample_Play(sshot, 0, 0);
}

int isrp() {
  if (pressr == 1) {
    return 1;
  } else {
    return 0;
  }
}

int islp() {
  if (pressl == 1) {
    return 1;
  } else {
    return 0;
  }
}

int isjp() {
  if (pressj == 1) {
    return 1;
  } else {
    return 0;
  }
}

int isnp() {
  if (pressn == 1) {
    return 1;
  } else {
    return 0;
  }
}

void setgameseq(int value) {
  gameseq = value;
}

static void keyboard(unsigned char key, int x, int y)
{
  switch (key) {
  case '\033':

    exit(0);
  case 'l':
    pressr = 1;
    break;
  case 'k':
    pressl = 1;
    break;
  case 'j':
    pressj = 1;
    break;
  case 'n':
    pressn = 1;
    break;
  default:
    break;
  }
}

static void keyboardUp(unsigned char key, int x, int y) {
  switch (key) {
  case 'l':
    pressr = 0;
    break;
  case 'k':
    pressl = 0;
    break;
  case 'j':
    pressj = 0;
    break;
  case 'n':
    pressn = 0;
    break;
  default:
    break;
  }
}

void setfade(int m) {
  fademode = m;
}

int getfade() {
  return fademode;
}

void settextspeed(int n) {
  textspeed = n;
}

int updatefade() {
  int ret = 0;
  int n;
  int m;
  float np;
  float mp;
  int flag;

  if (fademode == 1) {
    if (fadeint < 21) {
      fadeint++;
    } else {
      ret = 1;
    }
  } else {
    if (fadeint > 0) {
      fadeint--;
    } else {
      ret = 1;
    }
  }
  if (fadeint > 0) {
  glDisable(GL_LIGHTING);
  for (n = 0; n < 4; n++) {
    for (m = 0; m < 5; m++) {
      if (flag && fadeint < 20) {
        flag = 0;
        continue;
      } else {
        flag = 1;
      }
      if (fadeint > n*m) {
        np = -2 + (float)n;
        mp = -1.3 + (float)m / 2;
        glColor4f(0.2, (float)m/5, 0.2, 0);
        glNormal3d(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
	      glVertex3f(np, mp, 1.0);
	      glVertex3f(np+1, mp, 1.0);
	      glVertex3f(np+1, mp+0.5, 1.0);
        glVertex3f(np, mp+0.5, 1.0);
	      glEnd();
      }
    }
  }
  glEnable(GL_LIGHTING);
  }

  return ret;
}

static void resize(int w, int h)
{

  glViewport(0, 0, w, h);
  

  glMatrixMode(GL_PROJECTION);
  

  glLoadIdentity();
  gluPerspective(60.0, (double)w / (double)h, 1.0, 100.0);
}

void timer(int value) {
  if (!notimer) {
    MikMod_Update();
    if (!mmload) {
      Player_SetVolume(mvolume);
      stext->volume = mvolume / 12;
      mmload = 1;
    }
  }
	glutPostRedisplay();
	glutTimerFunc(16, timer, 0);
  /* counter1 += 0.01; */
}

static void glinit() {
  init();
  glutDisplayFunc(display);
  glutReshapeFunc(resize);
  glutTimerFunc(100, timer, 0);
  glutKeyboardFunc(keyboard);
  glutKeyboardUpFunc(keyboardUp);
}