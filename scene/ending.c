/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                  Game Ends                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloade = 0;
GLubyte *txendbg;
float estrpos = 0;
int estrnum = 91;

void loadetex() {
    unsigned char *tex = loadrle("rle/ending");
    txendbg = (GLubyte *)malloc(1024*1024*4);
    memcpy(txendbg, tex, 1024*1024*4);
    free(tex);
}

void loadesnd() {
    loadmodule("music/eo.mod", 1);
}

void drawedodeca() {
    int j;
    float x = 0;
    float y = 0;
    float z = 0;
    float coss = 0.309;
    float cost = 0.809;
    float sins = 0.951;
    float sint = 0.587;
    float zbase = 0.01;
    float xi = (sqrt(5)+1) / 2;

    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glPushMatrix();
    glColor4f(1.0, 1.0, 0.0, 0);
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_TRIANGLES);
        glVertex3f(1, 0,  zbase); /* A, B, C */
        glVertex3f(coss, sins, zbase); 
        glVertex3f(-cost, sint, zbase);

        glVertex3f(-cost, sint, zbase); /* C, D, E */
        glVertex3f(-cost, -sint, zbase);
        glVertex3f(coss, -sins, zbase);

        glVertex3f(coss, -sins, zbase); /* E, C, A */
        glVertex3f(-coss, sins, zbase);
        glVertex3f(1, 0,  zbase);
    glEnd();
    glPopMatrix();
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
}

void drawebg() {
    glEnable(GL_BLEND);
    glPushMatrix();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, txendbg);
    glNormal3d(0.0, 0.0, 1.0);
    glEnable(GL_TEXTURE_2D);
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(-2.4, -2.4,  0.0);
    glTexCoord2d(1.0, 0.0);
    glVertex3d( 2.4, -2.4,  0.0);
    glTexCoord2d(1.0, 1.0);
    glVertex3d( 2.4,  2.4,  0.0);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(-2.4,  2.4,  0.0);
    glEnd();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
}

void eloadtext(int num) {
    int j;
    for (j = 0; j < 26; j++) {
        fontblit(messages[num][j], j);
    }
}

void eupdatetext() {
    int j;
    int k = 0;
    float stsine;

    stsine = sin(estrpos/17)/3;
    for(j = 0; j < 26; j++) {
        if (j > 13) {
            k = 1;
        }
        chardraw(j*0.3-1.5, estrpos/100+stsine+k, j);
    }
}

void escene() {
    if (isloade == 0) {
        setentext(0, 0);
        displaymessageset(0);
        loadetex();
        loadesnd();
        eloadtext(estrnum);
        setfade(0);
        isloade = 1;
    }

    if (isrp()) {
        setfade(1);
    }

    if (getfade() == 1 && updatefade() == 1) {
        endescene();
        return;
    } else {
        updatefade();
    }

    drawebg();
    eupdatetext();
    estrpos++;
    if (estrpos > 240) {
        estrnum++;
        if (estrnum > 94) {
            setfade(1);
        }
        eloadtext(estrnum);
        estrpos = 0;
    }
}

void endescene() {
    isloade = 0;
    estrpos = 0;
    setgameseq(5); /* to score */
    free(txendbg);
    stopsound();
}