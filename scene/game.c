/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                 Game Scene                                 */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloadg = 0;
float tgame = 0;
float momentx = 0.004;
float momenty = 0.012;
float bangle = 45;
/* x is horizontal(side), y is vertical(up) */
float ballx = 0;
float bally = 0;
float paddlex = 0;
float ballrotate;
float ballspeed = 2;
unsigned char ballhit = 0;
int balldeparture = 0;
int curstage = 1;
int gamemissflag = 0;
int gameoverflag = 0;
int gameclearflag = 0;
unsigned char gamelifeinit = 2;
unsigned char gamelife;
int gamescore = 11;
int gameitems[5] = {0, 0, 0, 0, 0};
/* items, 0=empty, 1=fast, 2=missile, 3=small */
float gameitemsx[5];
float gameitemsy[5];
float gameitemsmissilex[3] = {0, 0, 0};
float gameitemsmissiley[3] = {0, 0, 0};
int gameitemscounter[5] = {0, 0, 0, 0, 0};
int gameitemsbullet = 0;
float paddlewidth = 0.2;

const float ballareax1 = -2.1;
const float ballareax2 = 0.5;
const float ballareay1 = -1.6;
const float ballareay2 = 1.6;

unsigned char blockd[32][32];
unsigned char blocks[8][16][16];
int gameblocks[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int gameblocksclear[8] = {0, 0, 0, 0, 0, 0, 0, 0};

int stagetitletimer = 0;
int gamemisstimer = 0;
int dupflag = 0;

int gameseqnext = 0;

int jlock = 0;

GLubyte *txgamen[6];
GLubyte *txgamea[6];
GLubyte *txgameb[6];
GLubyte *txgamei[5];

/* 0=fn */
int texi[16];

int gamecheckblock = 0;

void loadgametex() {
    /* normal diff texture is 32x16 = 512 */
    unsigned char *tex;
    FILE *fp;
    switch (curstage) {
    case 1:
        tex = loadrle("rle/fn");
        txgamen[1] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamen[1], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/fa");
        txgamea[1] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamea[1], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/fb");
        txgameb[1] = (GLubyte *)malloc(512*512*4);
        memcpy(txgameb[1], tex, 512*512*4);
        free(tex);
        break;
    case 2:
        tex = loadrle("rle/sn");
        txgamen[2] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamen[2], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/sa");
        txgamea[2] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamea[2], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/sb");
        txgameb[2] = (GLubyte *)malloc(512*512*4);
        memcpy(txgameb[2], tex, 512*512*4);
        free(tex);
        break;
    case 3:
        tex = loadrle("rle/tn");
        txgamen[3] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamen[3], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/ta");
        txgamea[3] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamea[3], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/tb");
        txgameb[3] = (GLubyte *)malloc(512*512*4);
        memcpy(txgameb[3], tex, 512*512*4);
        free(tex);
        break;
    case 4:
        tex = loadrle("rle/qn");
        txgamen[4] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamen[4], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/qa");
        txgamea[4] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamea[4], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/qb");
        txgameb[4] = (GLubyte *)malloc(512*512*4);
        memcpy(txgameb[4], tex, 512*512*4);
        free(tex);
        break;
    case 5:
        tex = loadrle("rle/pn");
        txgamen[5] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamen[5], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/pa");
        txgamea[5] = (GLubyte *)malloc(512*512*4);
        memcpy(txgamea[5], tex, 512*512*4);
        free(tex);
        tex = loadrle("rle/pb");
        txgameb[5] = (GLubyte *)malloc(512*512*4);
        memcpy(txgameb[5], tex, 512*512*4);
        free(tex);
        break;
    }
    /* glGenTextures(1, &texi); */
    /* item textures */
    txgamei[1] = malloc(16*16*3);
    txgamei[2] = malloc(16*16*3);
    txgamei[3] = malloc(16*16*3);
    if ((fp = fopen("rle/ifast", "rb")) != NULL) {
      fread(txgamei[1], sizeof(GLubyte), 16*16*3, fp);
      fclose(fp);
    }
    if ((fp = fopen("rle/imissile", "rb")) != NULL) {
      fread(txgamei[2], sizeof(GLubyte), 16*16*3, fp);
      fclose(fp);
    }
    if ((fp = fopen("rle/ismall", "rb")) != NULL) {
      fread(txgamei[3], sizeof(GLubyte), 16*16*3, fp);
      fclose(fp);
    }
}

void loadgamesnd() {
    switch (curstage) {
        case 1:
            loadmodule("music/ingame.xm", 1);
            break;
        case 2:
            loadmodule("music/ingame.xm", 1);
            break;
        case 3:
            loadmodule("music/ingame.xm", 1);
            break;
        case 4:
            loadmodule("music/ingame.xm", 1);
            break;
        case 5:
            loadmodule("music/ingame.xm", 1);
            break;
    }
}

void loadgamemaps() {
    FILE *fp;
    unsigned char buffer;
    int i;
    int j;
    int k;

    if((fp = fopen("maps.txt", "rb")) == NULL) {
                fprintf(stderr, "map file open error");
                exit(1);
    }

    for (k = 0; k < 8; k++) {
        for (i = 0; i < 16; i++) {
            for (j = 0; j < 16; j++) {
                fread(&buffer, sizeof(char), 1, fp);
                blocks[k][i][j] = buffer - 48;
                if (blocks[k][i][j] == 0) {
                    gameblocks[k+1]++;
                }
            }
            fseek(fp, 2, SEEK_CUR);
        }
        fseek(fp, 3, SEEK_CUR);
    }
    for (k = 0; k < 256; k++) {
        for (i = 0; i < 8; i++) {
            fread(&buffer, sizeof(char), 1, fp);
            if (i == 1) {
                addcseq(k, i, buffer - 33);
            } else {
                addcseq(k, i, buffer - 48);
            }
        }
        if (feof(fp))
            break;
        fseek(fp, 2, SEEK_CUR);
    }
}

void gameitemsupdate() {
    int j;

    for (j = 0; j < 5; j++) {
        if (gameitems[j] > 0) {
            if (gameitemsy[j] > -1.38 && gameitemsy[j] < -1.36 && gameitemsx[j] > paddlex - 0.2 && gameitemsx[j] < paddlex + 0.2) {
                gameitemsget(j);
            }
            if (gameitemsy[j] > -1.6) {
                gameitemsy[j] = gameitemsy[j] - 0.01;
                gameitemsdisplay(j);
            } else {
                gameitemsclear(j);
            }
        }
        if (gameitemscounter[j] > 0) {
            if (gameitemscounter[j] < 10) {
                gameitemsover(j);
            }
            gameitemscounter[j]--;
        }
    }
    gameitemsmissileupdate();
    displayitemsstatus(gameitemscounter[0], gameitemscounter[1], gameitemscounter[2], 1, 0);
}

void gameitemsclear(int j) {
    if (gameitems[j] != 0) {
        gameitemsy[j] = 0;
        gameitemsx[j] = 0;
        gameitems[j] = 0;
    }
}

void gameitemsover(int j) {
    if (ballspeed != 2) {
        ballspeed = 0;
    }
    if (paddlewidth != 0.2) {
        paddlewidth = 0.2;
    }
    /*switch (gameitems[j]) {
        case 1:
            ballspeed = 2;
            break;
        case 3:
            paddlewidth = 0.2;
            break;
    }*/
}

void gameitemsget(int j) {
    switch (gameitems[j]) {
        case 1:
            ballspeed = 3;
            gameitemscounter[j] = 200;
            break;
        case 2:
            gameitemsbullet = 3;
            break;
        case 3:
            paddlewidth = 0.1;
            gameitemscounter[j] = 200;
            break;
    }
    gameitemsclear(j);
}

void gameitemsuse() {
    if (gameitemsbullet > 0) {
        gameitemsmissileset();
        playsshot();
        gameitemsbullet--;
    }
}

int gameitemsgetbullet() {
    return gameitemsbullet;
}

void gameitemsappear(int slot, int type, float x, float y) {
    gameitems[slot] = type;
    gameitemsx[slot] = x;
    gameitemsy[slot] = y;
}

void gameitemsdisplay(int j) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 16, 16, 0, GL_RGB, GL_UNSIGNED_BYTE, txgamei[gameitems[j]]);
    glEnable(GL_TEXTURE_2D);
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
        glVertex3f(gameitemsx[j], gameitemsy[j], 0.05);
    glTexCoord2d(1.0, 0.0);
        glVertex3f(gameitemsx[j]+0.1 , gameitemsy[j], 0.05);
    glTexCoord2d(1.0, 1.0);
        glVertex3f(gameitemsx[j]+0.1 , gameitemsy[j]+0.1, 0.05);
    glTexCoord2d(0.0, 1.0);
    glVertex3f(gameitemsx[j], gameitemsy[j]+0.1, 0.05);
    glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}

void gameitemsmissileupdate() {
    int j;

    for (j = 0; j < 3; j++) {
        if (gameitemsmissilex[j] != 0) {
            gameitemsmissiley[j] = gameitemsmissiley[j] + 0.02;
            gameitemsmissiledisplay(j);
            if (gameitemsmissiley[j] > ballareay2)
                gameitemsmissileclear(j);
        }
    }
}

void gameitemsmissiledisplay(int j) {
    glPushMatrix();
    glTranslatef(gameitemsmissilex[j], gameitemsmissiley[j], 0.1);
    glRotatef(270, 1, 0, 0);
    glNormal3d(0.0, 0.0, 1.0);
    glutSolidCone(0.05, 0.1, 6, 3);
    glPopMatrix();
}

int gameitemsmissilehitcheck(float blx, float bly, float h, float v) {
    int j;

    for (j = 0; j < 3; j++) {
        if (gameitemsmissilex[j] != 0 && gameitemsmissilex[j] > blx && gameitemsmissilex[j] < blx+h && gameitemsmissiley[j] > bly && gameitemsmissiley[j] < bly+v) {
            return j;
        }
    }
    return -1;
}

void gameitemsmissileclear(int l) {
    gameitemsmissilex[l] = 0;
    gameitemsmissiley[l] = 0;
}

void gameitemsmissileset() {
    int j;
    
    for (j = 0; j < 3; j++) {
        if (gameitemsmissilex[j] == 0) {
            gameitemsmissilex[j] = paddlex;
            gameitemsmissiley[j] = -1.35;
            break;
        }
    }
}

int dispstagenameupdate() {
    float py = 0.5;
    int i;
    int n = 0;

    if (stagetitletimer < 150) {
        /* bg draw */
        glPushMatrix();
        glDisable(GL_LIGHTING);
        glColor4f(0.0, 0.0, 0.0, 1);
        glNormal3d(0.0, 0.0, 1.0);
        glTranslatef(0.0, -1.35, 0.2);
        glRectf(0.0, 0.0, 0.4, 3);
        glEnable(GL_LIGHTING);
        glPopMatrix();

        /* str draw */
        /* it needs fontblit before .. */
        for (i = 0; i < 4; i++) {
            if (stagetitletimer < 25) { 
                fontblit(messages[96+curstage][3], 0);
            } else if (stagetitletimer < 50) {
                fontblit(messages[96+curstage][2], 1);
            } else if (stagetitletimer < 75) {
                fontblit(messages[96+curstage][1], 2);
            }  else if (stagetitletimer < 100) {
                fontblit(messages[96+curstage][0], 3);
            }
            chardraw(0.2, py*i, i);
        }
        stagetitletimer++;
        ennamestage(1);
        changestagenumber(curstage);
        return 0;
    }
    setentext(0, 0);
    return 1;
}

void setblock(int k) {
    int i;
    int j;

    for (i = 0; i < 16; i++) {
        for (j = 0; j < 16; j++) {
            blockd[j][i] = blocks[k][i][j];
        }
    }
}

void clearblock() {
    int i;
    int j;

    for (i = 0; i < 32; i++) {
        for (j = 0; j < 32; j++) {
            blockd[i][j] = 4;
            /* blockt[i][j] = 4; */
        }
    }
}

unsigned char gamedrawblocks(int wid, int hei) {
    int i;
    int j;
    unsigned char hit = 0;
    int k;
    int l = -1;

    float h = 2.6 / wid;
    float v = 3.2 / hei;

    float blx;
    float bly;
    float bmx;
    float bmy;

    float btrans;
    if (gamecheckblock) {
        glDisable(GL_BLEND);
    } else {
        glEnable(GL_BLEND);
    }
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, txgamen[curstage]);

    for (i = 0; i < hei; i++) {
        for (j = 0; j < wid; j++) {
            blx = (h*j)+ballareax1;
            bly = (v*-i)+ballareay2;
            if ((blockd[j][i] < 3 || (blockd[j][i] > 4 && blockd[j][i] < 10))
                && hit == 0 && (ballx > blx && ballx < blx+h && bally > bly && bally < bly+v)) {
                bmx = ballx - momentx;
                bmy = bally - momenty;
                if (blockd[j][i] > 4 && blockd[j][i] < 9) {
                    for(k = 0; k < 5; k++) {
                        if (gameitems[k] == 0) {
                            gameitemsappear(k, blockd[j][i] - 4, ballx, bally+momenty);
                            break;
                        }
                    } 
                    blockd[j][i] = 0;
                }
                if (blockd[j][i] != 9)
                    blockd[j][i]++;
                if (blockd[j][i] == 3) {
                    gameblocksclear[curstage]++;
                    gamescore = gamescore + 10;
                }
                if (bmx < blx) {hit = 1;}
                if (bmx > blx+h) {hit = 2;}
                if (bmy < bly) {hit = 3;}
                if (bmy > bly+v || hit == 0) {hit = 4;}
            }
            if ((blockd[j][i] < 3 || (blockd[j][i] > 4 && blockd[j][i] < 10))) {
                l = gameitemsmissilehitcheck(blx, bly, h, v);
                if (l > -1) {
                    blockd[j][i] = 4;
                    gameblocksclear[curstage]++;
                    gameitemsmissileclear(l);
                }
            }
            if (blockd[j][i] < 3 || (blockd[j][i] > 4 && blockd[j][i] < 10)) {
                if (blockd[j][i] < 3) {
                    btrans = 1 - (blockd[j][i]) * 0.3;
                } else {
                    btrans = 1;
                }
                glPushMatrix();
                glEnable(GL_TEXTURE_2D);
                glDisable(GL_LIGHTING);
                glColor4f(1.0, 1.0, 1.0, btrans);
                glNormal3d(0.0, 0.0, 1.0);
                glBegin(GL_QUADS);
                glTexCoord2d(0.0+0.0625*j, 0.0+0.0625*(16-i));
                glVertex3d(blx, bly, 0.0);
                glTexCoord2d(0.0625+0.0625*j, 0.0+0.0625*(16-i));
                glVertex3d(blx+h, bly, 0.0);
                glTexCoord2d(0.0625+0.0625*j, 0.0625+0.0625*(16-i));
                glVertex3d(blx+h, bly+v, 0.0);
                glTexCoord2d(0.0+0.0625*j, 0.0625+0.0625*(16-i));
                glVertex3d(blx,  bly+v, 0.0);
                glEnd();
                glDisable(GL_TEXTURE_2D);
                glEnable(GL_LIGHTING);
                glPopMatrix();
            }
        }
    }
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D); /* Only use this if not using shaders. */

    return hit;
}

void gamedrawbg() {
    glEnable(GL_BLEND);
    glPushMatrix();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, txgameb[curstage]);
    glNormal3d(0.0, 0.0, 1.0);
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glTexCoord2d(1.0, 0.0);
    glVertex3d(2.4, -2.4,  -0.01);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(-2.4, -2.4,  -0.01);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(-2.4, 2.4,  -0.01);
    glTexCoord2d(1.0, 1.0);
    glVertex3d(2.4, 2.4,  -0.01);
    glEnd();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
}

void gamedrawscores() {
    displayscores(curstage, gamelife, gamescore, 1, 1);
}

void endgamescene() {
    int j;
    setentext(0, 0);
    ballx = 0;
    bally = 0;
    balldeparture = 0;
    stagetitletimer = 0;
    free(txgamen[curstage]);
    free(txgamea[curstage]);
    free(txgameb[curstage]);
    for (j = 0; j < 3; j++)
        free(txgamei[j]);
    isloadg = 0;
    /* to next scene */
    setgameseq(gameseqnext);
    if (gamemissflag == 0 || gameoverflag == 1 || gameclearflag == 1) {
        stopsound();
    }
    if (gameclearflag == 1) {
        curstage++;
    }
}

int gamemissupdate() {
    int i;

    if (gamemissflag == 1 || gameclearflag == 1) {
        if (gamemisstimer < 150) {
            /* bg draw */
            glPushMatrix();
            glDisable(GL_LIGHTING);
            glColor4f(0.0, 0.0, 0.0, 1);
            glNormal3d(0.0, 0.0, 1.0);
            glTranslatef(0.0, -1.35, 0.2);
            glRectf(0.0, 0.0, 0.4, 3);
            glEnable(GL_LIGHTING);
            glPopMatrix();

            for (i = 0; i < 5; i++) {
                if (gamemisstimer < 5) { 
                    if (gameclearflag == 1) {
                        fontblit(0x2352, 0);
                    } else {
                        fontblit(0x2353, 0);
                    }
                } else if (gamemisstimer < 10) {
                    if (gameclearflag == 1) {
                        fontblit(0x2341, 1);
                    } else {
                        fontblit(0x2353, 1);
                    }
                } else if (gamemisstimer < 15) {
                    if (gameclearflag == 1) {
                        fontblit(0x2345, 2);
                    } else {
                        fontblit(0x2349, 2);
                    }
                }  else if (gamemisstimer < 20) {
                    if (gameclearflag == 1) {
                        fontblit(0x234c, 3);
                    } else {
                        fontblit(0x234d, 3);
                    }
                }  else if (gamemisstimer < 25) {
                    if (gameclearflag == 1) {
                        fontblit(0x2343, 4);
                    }
                }
                chardraw(0.2, 0.5*i, i);
            }
            gamemisstimer++;
        } else {
            setfade(1);
        }
        return 0;
    } else {
        return 1;
    }
}

void gamescene() {
    float sine;
    float clearrate;

    /* game init */
    if (isloadg == 0) {
        loadgametex();
        if (gamemissflag == 0 || gameoverflag == 1) {
            loadgamesnd();
            loadgamemaps();
            clearblock();
            /* setblock() = stage number */
            setblock(curstage - 1);
        }
        if (gameclearflag == 0 && gamemissflag == 0) {
            gamelife = gamelifeinit;
            gamescore = 11;
        }
        setfade(0);
        setentext(0, 1);
        displaymessageclear();
        gamemissflag = 0;
        gamemisstimer = 0;
        gameoverflag = 0;
        gameclearflag = 0;
        isloadg = 1;
    }

    /* inputs */
    if (isjp()) {
        jlock = 1;
    } else {
        if (jlock == 1) {
            gameitemsuse();
        }
        jlock = 0;
    }
    if (isnp()) {
        balldeparture = 1;
        gamecheckblock = 1;
    } else {
        gamecheckblock = 0;
    }
    if (islp()) {
        if (paddlex > -1.85) {
            paddlex = paddlex - 0.08;
        }
    }
    if (isrp()) {
        if (paddlex < 0.3) {
            paddlex = paddlex + 0.08;
        }
    }

    /* is miss */
    if (bally < -1.4 && gamemissflag == 0) {
        displaymessageclear();
        if (gamelife > 0) {
            gamelife = gamelife - 1;
            /* to game */
            gameseqnext = 1;
        } else {
            gameoverflag = 1;
            /* to score */
            gameseqnext = 5;
        }
        gamemissflag = 1;
    }
    /* is clear */
    clearrate = (float)gameblocksclear[curstage] / (float)gameblocks[curstage];
    if (gameclearflag == 0 && clearrate > 0.5) {
        displaymessageclear();
        gameclearflag = 1;
        gameseqnext = 2;
    }

    /* game miss and clear fade */
    gamemissupdate();

    /* ball movement */
    if (dispstagenameupdate()) {
        if (ballx > ballareax2 || ballhit == 1) {
            bangle = 360 - bangle;
        } else if (ballx < ballareax1 || ballhit == 2) {
            bangle = 360 - bangle;
        }
        if (bally > ballareay2 || ballhit == 3) {
            bangle = 180 - bangle;
        /* } else if (bally < ballareay1 || ballhit == 4) { */
        } else if (ballhit == 4) {
            bangle = 180 - bangle;
        }
        if (ballx > ballareax2) { ballx = ballareax2 - (ballspeed / 200); dupflag = 1;}
        if (ballx < ballareax1) { ballx = ballareax1 + (ballspeed / 200); dupflag = 2;}
        if (bally > ballareay2) { bally = ballareay2 - (ballspeed / 200); dupflag = 3;}
        /* if (bally < ballareay1) { bally = ballareay1 + (ballspeed / 200); dupflag = 4;} */
        if (ballx > paddlex-paddlewidth && ballx < paddlex+paddlewidth && bally < -1.36 && bally > -1.39) {
            if (bally - momenty < -1.36 || bally - momenty > -1.39)
                bangle = 180 - bangle;
            bangle = bangle + (ballx - paddlex) * 90;
        }
        momentx = sin(bangle * 3.14 / 180) * (ballspeed / 100);
        momenty = cos(bangle * 3.14 / 180) * (ballspeed / 100);
        ballx = ballx + momentx;
        bally = bally + momenty;
    }
    if (balldeparture == 0) {
     ballx = paddlex;
     bally = -1.35;
    }

    /* draw ball ... by using library func */
    glPushMatrix();


    /* translate */
    glTranslatef(ballx, bally, 0.1);
    /* rotate */
    ballrotate = ballrotate + ballspeed*5;
    glRotatef(ballrotate, -momenty, momentx, 0);
    glutSolidSphere(0.05, 10, 10);
    glPopMatrix();


    /* draw wall */
    /*
    glPushMatrix();

    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(-3, ballareay1,  0.0);
    glTexCoord2d(1.0, 0.0);
    glVertex3d(ballareax1, ballareay1,  0.0);
    glTexCoord2d(1.0, 1.0);
    glVertex3d(ballareax1, ballareay2,  0.0);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(-3,  ballareay2,  0.0);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glRectf(ballareax1, ballareay2, ballareax2, 2);
    glPopMatrix();

    glPushMatrix();
    glRectf(ballareax2, ballareay2, 1, ballareay1);
    glPopMatrix();
 
    glPushMatrix();
    glRectf(ballareax1, ballareay1, ballareax2, -2);
    glPopMatrix();
    */

    /* draw scores */
    gamedrawscores();

    gameitemsupdate();

    /* character after */
    glEnable(GL_BLEND);
    glPushMatrix();
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, txgamea[curstage]);
    glNormal3d(0.0, 0.0, 1.0);
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glTexCoord2d(1.0, 0.0);
    glVertex3d(ballareax2, ballareay1,  -0.01);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(ballareax1, ballareay1,  -0.01);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(ballareax1, ballareay2,  -0.01);
    glTexCoord2d(1.0, 1.0);
    glVertex3d(ballareax2,  ballareay2,  -0.01);
    glEnd();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);

    /* paddle */
    glPushMatrix();

    glNormal3d(0.0, 0.0, 1.0);
    glTranslatef(paddlex, -1.45, 0.0);
    glScalef(paddlewidth*10, 0.2, 0.2);
    glutSolidCube(0.2);
    /* glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(0.175, -0.02, 0.2);
    glTexCoord2d(1.0, 0.0);
    glVertex3d(-0.175, -0.02, 0.2);
    glTexCoord2d(1.0, 1.0);
    glVertex3d(-0.175, 0.02, 0.2);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(0.175, 0.02, 0.2);
    glEnd(); */
    glPopMatrix();

    /* draw blocks & check collision */
    ballhit = gamedrawblocks(16, 16);
    if (ballhit > 0)
        gamescore++;

    if (getfade() == 1 && updatefade() == 1) {
        endgamescene();
        return;
    } else {
        updatefade();
    }

    /* draw bg */
    /* glNormal3d(0.0, 0.0, 1.0);
    glRectf(-1.9, 1.4, 0.5, -1.4);
    */
   gamedrawbg();

    tgame = tgame + 1;
}