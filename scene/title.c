/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                    Title                                   */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloadt = 0;
int isrp();
int islp();
void setgameseq();
int notimer;
int titleselect = 0;
int titlenext = 1;
int llock;

/* GLubyte* txtitlebg[1024][1024][4]; */
GLubyte *txtitlebg;

void loadtitletex() {
        unsigned char *tex = loadrle("rle/title");
        txtitlebg = (GLubyte *)malloc(1024*1024*4);
        memcpy(txtitlebg, tex, 1024*1024*4);
        free(tex);
}

void loadtitlesnd() {
    loadmodule("music/start.xm", 1);
}

void endtitlescene() {
    /* setentext(0, 0); */
    settextspeed(3);
    displaymessageclear();
    free(txtitlebg);
    isloadt = 0;
    setgameseq(titlenext);
    stopsound();
}

void titleselectupdate() {
  if (titleselect == 0) {
    messages[3][0] = 8804;
    messages[3][13] = 0;
    messages[3][20] = 0;
    titlenext = 2;
  } else if (titleselect == 1) {
    messages[3][0] = 0;
    messages[3][13] = 8804;
    messages[3][20] = 0;
    titlenext = 4;
  } else {
    messages[3][0] = 0;
    messages[3][13] = 0;
    messages[3][20] = 8804;
    titlenext = 5;
  }
}

void titlescene() {
    FILE *fp;
    if (isloadt == 0) {
        settextspeed(5);
        loadtitletex();
        loadtitlesnd();
        stext = Sample_Load("sound/text.wav");
        stext->volume = 8;
        notimer = 0;
        setfade(0);
        setentext(1, 0);
        isloadt = 1;
        displaymessageset(3);
    }

  /* gfx_write */
  static const GLfloat color[] = { 1.0, 1.0, 1.0, 1.0 };  /* 材質 (色) */
  
  /* 材質の設定 */
  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
  
  if (isrp()) {
    setfade(1);
  }
  if (islp()) {
    llock = 1;
  } else {
    if (llock == 1) {
      if (titleselect < 2) {
        titleselect++;
      } else {
        titleselect = 0;
      }
      displaymessageset(3);
    }
    llock = 0;
  }

  if (getfade() == 1 && updatefade() == 1) {
    endtitlescene();
    return;
  } else {
    updatefade();
  }
  
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
  /* テクスチャマッピング開始 */
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, txtitlebg);
  glEnable(GL_TEXTURE_2D);
  
  /* draw title bg */
  glNormal3d(0.0, 0.0, 1.0);
  glBegin(GL_QUADS);
  glTexCoord2d(0.0, 0.0);
  glVertex3d(-2.4, -2.4,  0.0);
  glTexCoord2d(1.0, 0.0);
  glVertex3d( 2.4, -2.4,  0.0);
  glTexCoord2d(1.0, 1.0);
  glVertex3d( 2.4,  2.4,  0.0);
  glTexCoord2d(0.0, 1.0);
  glVertex3d(-2.4,  2.4,  0.0);
  glEnd();

  titleselectupdate();

  glBindTexture(GL_TEXTURE_2D, 0);

  glDisable(GL_BLEND);

  /* gfx write end */

}

