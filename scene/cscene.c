/*----------------------------------------------------------------------------*/
/*                                                                            */
/*                                  Cutscene                                  */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloadc = 0;
GLubyte *txcscenebg;
int cbgmode = 0;
float ctimer = 0;
GLubyte cgrad[256][3];
int llock = 0;
int nlock = 0;

int cdispchara[2];
GLubyte *txchara[8];
float ccfpos = -2;
float ccspos = 2;
int cenchara = 0;
int enchara = 1;
int clcharai = 0;
int crcharai = 1;
int cnextscene = 3;

unsigned char cseq[256][8];
int cpos = 0;

void loadcuttex() {
    unsigned char *tex;
    txcscenebg = (GLubyte *)malloc(64*64*3);
    tex = loadrle("rle/kzkn");
    txchara[0] = (GLubyte *)malloc(256*256*4);
    memcpy(txchara[0], tex, 256*256*4);
    free(tex);
    tex = loadrle("rle/savg");
    txchara[1] = (GLubyte *)malloc(256*256*4);
    memcpy(txchara[1], tex, 256*256*4);
    free(tex);
    tex = loadrle("rle/itar");
    txchara[0] = (GLubyte *)malloc(256*256*4);
    memcpy(txchara[0], tex, 256*256*4);
    free(tex);
    tex = loadrle("rle/mips");
    txchara[1] = (GLubyte *)malloc(256*256*4);
    memcpy(txchara[1], tex, 256*256*4);
    free(tex);
}

void loadcutsnd() {
    loadmodule("music/cscene.xm", 1);
}

void endcut() {
    free(txcscenebg);
    free(txchara[0]);
    free(txchara[1]);
    llock = 0;
    ctimer = 0;
    setgameseq(cnextscene);
    isloadc = 0;
    /*stopsound(); */
}

void addcseq(int row, int ele, unsigned char val) {
    /* 0 = character appear, 0=none, 1=left, 2=right, 3=both 
       1 = message number
       2 = character enable, 0=none, 1=left, 2=right, 3=both
       3 = character left image
       4 = character right image
       5 = name number
       6 = background, 0=plasma, 1=xor, 2=bw
       7 = next scene, 0=none, 9=title
    */
    cseq[row][ele] = val;
}

void updatecutchara() {
    if (cenchara == 1) {
        if (ccfpos < -1.5) {
            ccfpos = ccfpos + 0.1 * -(1.5 + ccfpos);
        }
        if (ccspos < 2) {
            ccspos = ccspos + 0.1 * (2 - ccspos);
        }
    } else if (cenchara == 2) {
        if (ccfpos > -2) {
            ccfpos = ccfpos - 0.1 * (2 + ccfpos);
        }
        if (ccspos > 1.5) {
            ccspos = ccspos - 0.1 * -(1.5 - ccspos);
        }
    } else if (cenchara == 3) {
        if (ccfpos < -1.5) {
            ccfpos = ccfpos + 0.1 * -(1.5 + ccfpos);
        }
        if (ccspos > 1.5) {
            ccspos = ccspos - 0.1 * -(1.5 - ccspos);
        }
    } else {
        if (ccfpos > -2) {
            ccfpos = ccfpos - 0.1 * (2 + ccfpos);
        }
        if (ccspos < 2) {
            ccspos = ccspos + 0.1 *  (2 - ccspos);
        }
    }
}

void dispchara() {
    if (enchara == 1 || enchara == 3) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glPushMatrix();
    
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, txchara[clcharai]);
        glEnable(GL_TEXTURE_2D);

        glDisable(GL_LIGHTING);
        glColor4f(1.0, 1.0, 1.0, 0.5+(2+ccfpos));

        glTranslatef(ccfpos, 0.1, -0.05);

        glNormal3d(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
        glTexCoord2d(0.0, 0.0);
        glVertex3d(-1, -1,  0.0);
        glTexCoord2d(1.0, 0.0);
        glVertex3d( 1, -1,  0.0);
        glTexCoord2d(1.0, 1.0);
        glVertex3d( 1,  1,  0.0);
        glTexCoord2d(0.0, 1.0);
        glVertex3d(-1,  1,  0.0);
        glEnd();

        glEnable(GL_LIGHTING);

        glPopMatrix();

        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_BLEND);
    }
    if (enchara == 2 || enchara == 3) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glPushMatrix();
    
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, txchara[crcharai]);
        glEnable(GL_TEXTURE_2D);

        glDisable(GL_LIGHTING);
        glColor4f(1.0, 1.0, 1.0, 0.5+(2-ccspos));

        glTranslatef(ccspos, 0.1, -0.05);

        glNormal3d(0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
        glTexCoord2d(1.0, 0.0);
        glVertex3d(-1, -1,  0.0);
        glTexCoord2d(0.0, 0.0);
        glVertex3d( 1, -1,  0.0);
        glTexCoord2d(0.0, 1.0);
        glVertex3d( 1,  1,  0.0);
        glTexCoord2d(1.0, 1.0);
        glVertex3d(-1,  1,  0.0);
        glEnd();

        glEnable(GL_LIGHTING);

        glPopMatrix();

        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_BLEND);
    }
}

void cutscene() {
    int i;
    int j;
    GLubyte pat;
    unsigned char n;
    GLubyte cr;
    GLubyte cg;
    GLubyte cb;

    if (isloadc == 0) {
        setentext(1, 1);
        displaymessageset(cseq[cpos][1]);
        loadcutsnd();
        loadcuttex();
        setfade(0);
        isloadc = 1;
    }


    if (!islp() && llock == 1) {
        /* setfade(1); */
    }

    if (islp()) {
        llock = 1;
    }

    if (isnp()) {
        nlock = 1;
    } else {
        if (nlock == 1 && getfade() != 1) {
            cpos++;
            cenchara = cseq[cpos][0];
            displaymessageset(cseq[cpos][1]);
            enchara = cseq[cpos][2];
            clcharai = cseq[cpos][3];
            crcharai = cseq[cpos][4];
            updatename(cseq[cpos][5]);
            cbgmode = cseq[cpos][6];
            if (cseq[cpos][7] != 0) {
                cnextscene = cseq[cpos][7];
                setfade(1);
                cpos++;
            }
        }
        nlock = 0;
    }

    if (getfade() == 1 && updatefade() == 1) {
        endcut();
        return;
    } else {
        updatefade();
    }

    ctimer = ctimer + 0.1;

    if (cbgmode == 1) {
        for (i = 0; i < 64; i++) {
            for (j = 0; j < 64; j++) {
                n = ((i ^ j) + ctimer) * 4;
                if (n < 256/6) { cr = 255; cg = n*6; cb = 0; } 
                else if (n < 256/6*2) { cr = 255-(n-256/6)*6; cg = 255; cb = 0; }
                else if (n < 256/6*3) { cr = 0; cg = 255; cb = (n-256/6*2)*6; }
                else if (n < 256/6*4) { cr = 0; cg = 255-(n-256/6*3)*6; cb = 255; }
                else if (n < 256/6*5) { cr = (n-256/6*4)*6; cg = 0; cb = 255; }
                else { cr = 255; cg = 0; cb = 255-(n-256/6*5)*6; }
                if (n > 250) { cb = 255 - n; }
                txcscenebg[i*64*3+j*3+0] = cr;
                txcscenebg[i*64*3+j*3+1] = cg;
                txcscenebg[i*64*3+j*3+2] = cb;
                /* txcscenebg[i*64*4+j*4+3] = 255; */
            }
        }
    } else if (cbgmode == 2) {
        for (i = 0; i < 64; i++) {
            for (j = 0; j < 64; j++) {
                n = 128+128*sin(i*j+ctimer);
                txcscenebg[i*64*3+j*3+0] = n;
                txcscenebg[i*64*3+j*3+1] = n;
                txcscenebg[i*64*3+j*3+2] = n;
                /* txcscenebg[i*64*4+j*4+3] = 255; */
            }
        }
    } else {
        /* default: plasma */
        for (i = 0; i < 64; i++) {
            for (j = 0; j < 64; j++) {
                pat = 128+128*((sin(sqrt((i-48)*(i-48)+(j-48)*(j-48)+ctimer))+sin(sqrt((i-16)*(i-16)+(j-16)*(j-16)+ctimer))+sin(i+ctimer))/3);
                txcscenebg[i*64*3+j*3+0] = 128 + 128 * sin((3.1415 * pat / 512) + ctimer / 16);
                txcscenebg[i*64*3+j*3+1] = 128 + 128 * sin((3.1415 * pat / 512) + ctimer / 32);
                txcscenebg[i*64*3+j*3+2] = 128 + 128 * sin((3.1415 * pat / 512) + ctimer / 24);
                /* txcscenebg[i*64*4+j*4+3] = 255; */
            }
        }
    }


    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, txcscenebg);
    glEnable(GL_TEXTURE_2D);
  
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(-2.4, -2.4,  -0.1);
    glTexCoord2d(1.0, 0.0);
    glVertex3d( 2.4, -2.4,  -0.1);
    glTexCoord2d(1.0, 1.0);
    glVertex3d( 2.4,  2.4,  -0.1);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(-2.4,  2.4,  -0.1);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_BLEND);

    updatecutchara();
    dispchara();
}