#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloado = 0;
int optionselect = 0;
int optionstereo = 0;
int optionfps = 0;
int optionvolume = 6;
int notimer = 0;
int rlock;
int mmload;
GLubyte *txoptionbg;

void loadoptiontex() {
        unsigned char *tex = loadrle("rle/optionbg");
        txoptionbg = (GLubyte *)malloc(1024*1024*4);
        memcpy(txoptionbg, tex, 1024*1024*4);
        free(tex);
}

void loadoptionsnd() {
    loadmodule("music/fa.xm", 1);
}

void optiondisplaybg() {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, txoptionbg);
    glEnable(GL_TEXTURE_2D);
    
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(-2.4, -2.4,  0.0);
    glTexCoord2d(1.0, 0.0);
    glVertex3d( 2.4, -2.4,  0.0);
    glTexCoord2d(1.0, 1.0);
    glVertex3d( 2.4,  2.4,  0.0);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(-2.4,  2.4,  0.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);

    glDisable(GL_TEXTURE_2D);
}

void optiondisplaychar() {
    int j;
    float k = 0;
    float l = 0;
    for (j = 0; j < 25; j++) {
        chardraw(l/3-1.5, 1.5-k, j);
        l++;
        if(j == 2 || j == 12 || j == 22) {
            k = k + 0.5;
            l = 0;
        }
    }
}

void optiondisplayvolume() {
    displayscore(optionvolume*10+1, 0.64, 0.64);
}

void optiondisplaycursor() { 
    float x = -2.1;
    switch (optionselect) {
        case 1:
            if (optionstereo == 1)
                x = -0.3;
            break;
        case 2:
            if (optionfps == 1) {
                x = 0.8;
            } else {
                x = 0.15;
            }
            break;
    }
    displaycursor(x, 0.8-optionselect*0.55);
}

void optionchangeselect(int p, int m) {
    switch (optionselect) {
        case 0:
            if (p > 0 && optionvolume < 9) {
                optionvolume++;
            }
            if (m > 0 && optionvolume > 1) {
                optionvolume--;
            }
            setvolume(optionvolume * 12);
            break;
        case 1:
            if (optionstereo == 0) {
                optionstereo = 1;
            } else {
                optionstereo = 0;
            }
            break;
        case 2:
            if (optionfps == 0) {
                optionfps = 1;
            } else {
                optionfps = 0;
            }
            break;
        case 3:
            setfade(1);
            stopsound();
            notimer = 1;
            break;
    }
}

void loadoptionmessage() {
    int j;
    for (j = 0; j < 26; j++) {
        fontblit(messages[4][j], j);
    }
}

void optionscene() {
    if (isloado == 0) {
        loadoptiontex();
        loadoptionsnd();
        setfade(0);
        setentext(0, 0);
        loadoptionmessage();
        isloado = 1;
    }

    optiondisplaybg();
    optiondisplaychar();
    optiondisplaycursor();
    optiondisplayvolume();

    if (isrp()) {
        rlock = 1;
    } else {
        if (rlock == 1) {
            optionchangeselect(0, 1);
        }
        rlock = 0;
    }
    if (islp()) {
        llock = 1;
    } else {
        if (llock == 1) {
            optionselect++;
            if (optionselect > 3)
                optionselect = 0;
        }
        llock = 0;
    }
    if (isnp()) {
        nlock = 1;
    } else {
        if (nlock == 1) {
            optionchangeselect(1, 0);
        }
        nlock = 0;
    }

    if (getfade() == 1 && updatefade() == 1) {
        endoptionscene();
        return;
    } else {
        updatefade();
    }
}

void endoptionscene() {
    isloado = 0;
    setgameseq(0); /* to title */
    free(txoptionbg);
    /* stopsound(); */
    mmload = 0;
    mikmodreset(optionstereo);
}