#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloads = 0;
GLubyte *txscorebg;
unsigned int highscores[5];
unsigned short hiscorename[3] = {9025, 9025, 9025};
unsigned char scoreblinktimer = 0;
char scoreinputselect = 0;
unsigned char isblinkload[3] = {0, 0, 0};
char scoreinputlock[3] = {0, 0, 0};
int scoremode;
int scoreinput = 0;

void loadscoretex() {
        unsigned char *tex = loadrle("rle/scorebg");
        txscorebg = (GLubyte *)malloc(1024*1024*4);
        memcpy(txscorebg, tex, 1024*1024*4);
        free(tex);
}

void loadscoresnd() {
    loadmodule("music/mac22351.mod", 1);
}

void loadscores() {
    FILE *fp;
    int j;
    int k;
    int l = 0;
    char buffer;
    for (j = 0; j < 5; j++) {
        highscores[j] = 0;
    }
    if ((fp = fopen("scores.txt", "rb")) != NULL) {
    } else {
        printf("failed to read scores");
        exit(0);
    }
    for (j = 0; j < 5; j++) {
        for (k = 0; k < 11; k++) {
            fread(&buffer, sizeof(char), 1, fp);
            if (k < 3) {
                messages[6][l] = buffer + 8960;
                fontblit(messages[6][l], 8 + l);
                l++;
            } else {
                highscores[j] = highscores[j] + (buffer - 48) * disppow(10, 8-(k-2));
            }
        }
        highscores[j] = highscores[j] / 10;
        fseek(fp, 2, SEEK_CUR);
    }
    fclose(fp);
}

void scoredrawtitle(int mode) {
    switch (mode) {
        case 0:
            /* highscore */
            if (isloads == 0) {
                fontblit(messages[5][0], 1);
                fontblit(messages[5][1], 2);
                fontblit(messages[5][2], 3);
                fontblit(messages[5][3], 4);
                fontblit(messages[5][4], 5);
                scoreinput = 1;
            }
            chardraw(-1, 1.9, 1);
            chardraw(-0.6, 1.9, 2);
            chardraw(-0.2, 1.9, 3);
            chardraw(0.2, 1.9, 4);
            chardraw(0.6, 1.9, 5);
            break;
        case 1:
            /* score */
            if (isloads == 0) {
                fontblit(messages[5][6], 1);
                fontblit(messages[5][7], 2);
                fontblit(messages[5][8], 3);
            }
            chardraw(-1, 1.9, 1);
            chardraw(-0.6, 1.9, 2);
            chardraw(-0.2, 1.9, 3);
            break;
        default:
            /* game over */
            if (isloads == 0) {
                fontblit(messages[5][10], 1);
                fontblit(messages[5][11], 2);
                fontblit(messages[5][12], 3);
                fontblit(messages[5][13], 4);
                fontblit(messages[5][14], 5);
                fontblit(messages[5][15], 6);
                fontblit(messages[5][16], 7);
            }
            chardraw(-1, 1.9, 1);
            chardraw(-0.6, 1.9, 2);
            chardraw(-0.2, 1.9, 3);
            chardraw(0.2, 1.9, 4);
            chardraw(0.6, 1.9, 5);
            chardraw(1, 1.9, 6);
            chardraw(1.4, 1.9, 7);
            break;
    }
}

void scoredrawscores(float x, float y) {
    int j;
    int k;
    int l = 8;
    /* draw names */
    for (j = 0; j < 5; j++) {
        for (k = 0; k < 3; k++) {
            chardraw(k*0.3+x, j*0.3+y, l);
            l++;
        }
        displayscore(highscores[j], 2+x, j*0.227-0.525+y);
    }
}

void scoretextupdate() {
    /* title */
    scoredrawtitle(scoremode);
    /* scores */
    scoredrawscores(-1.5, 0);
}

void scorehighupdate() {
    int j;
    for (j = 0; j < 3; j++) {
        if (scoreinputselect == j && scoreblinktimer > 200) {
            if (isblinkload[j] == 1) {
                fontblit(8481, 23+j);
                isblinkload[j] = 0;
            }
        } else {
            if (isblinkload[j] == 0) {
                fontblit(hiscorename[j], 23+j);
                isblinkload[j] = 1;
            }
        }
    }
    chardraw(-1.5, -0.5, 23);
    chardraw(-1.2, -0.5, 24);
    chardraw(-0.9, -0.5, 25);
    displayscore(gamescore, 0.5, -0.9);
}
int writehighscore(int enwrite) {
    int j;
    int k;
    FILE *fp;
    unsigned char buf[3];
    unsigned char bufscore[8];
    for (j = 4; j > -1; j--) {
        if(highscores[j] < gamescore) {
            break;
        }
    }
    if (j == -1) {
        return 1;
    }
    if (!enwrite) {
        return 0;
    }
    for (k = 0; k < j; k++) {
        highscores[k] = highscores[k+1];
        messages[6][k*3] = messages[6][(k+1)*3];
        messages[6][k*3+1] = messages[6][(k+1)*3+1];
        messages[6][k*3+2] = messages[6][(k+1)*3+2];
    }
    highscores[j] = gamescore;
    messages[6][j*3] = hiscorename[0];
    messages[6][j*3+1] = hiscorename[1];
    messages[6][j*3+2] = hiscorename[2];

    if (enwrite) {
    /* write to file */
        fp = fopen("scores.txt", "wb");
        for (k = 0; k < 5; k++) {
            buf[0] = messages[6][k*3] - 8960;
            buf[1] = messages[6][k*3+1] - 8960;
            buf[2] = messages[6][k*3+2] - 8960;
            fwrite(buf, 1, 3, fp);
            for (j = 7; j >= 0; j--) {
                bufscore[7-j] = digittochar(highscores[k], j);
            }
            fwrite(bufscore, 1, 8, fp);
            fwrite("\r\n", 1, 2, fp);
        }
        fclose(fp);
    }
    
    return 0;
}

void updatescoremode() {
    int j = writehighscore(0);
    /* only check */
    if (!j && gameoverflag == 1) {
        /* gameover */
        scoremode = 2;
    } else if (j) {
        /* score */
        scoremode = 1;
    } else {
        /* highscore */
        scoremode = 0;
    }
}

void scoredisplaybg() {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, txscorebg);
    glEnable(GL_TEXTURE_2D);
    
    glNormal3d(0.0, 0.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2d(0.0, 0.0);
    glVertex3d(-2.4, -2.4,  0.0);
    glTexCoord2d(1.0, 0.0);
    glVertex3d( 2.4, -2.4,  0.0);
    glTexCoord2d(1.0, 1.0);
    glVertex3d( 2.4,  2.4,  0.0);
    glTexCoord2d(0.0, 1.0);
    glVertex3d(-2.4,  2.4,  0.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);

    glDisable(GL_TEXTURE_2D);
}

void scorescene() {
    if (isloads == 0) {
        setentext(0, 0);
        displaymessageset(0);
        loadscoretex();
        loadscoresnd();
        loadscores();
        setfade(0);
        updatescoremode();
        setvolume(optionvolume * 10);
        if (gameoverflag) {
            cpos = 0;
            gamemissflag = 0;
            gameoverflag = 0;
        }
        scoredrawtitle(scoremode);
        scoreblinktimer = 0;
        isloads = 1;
    }

    if (isnp()) {
        setfade(1);
    }
    if (isjp()) {
        if (scoreinputlock[2] == 0) {
            hiscorename[scoreinputselect]++;
            if (hiscorename[scoreinputselect] > 9050) {
                hiscorename[scoreinputselect] = 9025;
            }
            isblinkload[scoreinputselect] = 0;
            scoreinputlock[2] = 1;
        }
    } else {
        scoreinputlock[2] = 0;
    }
    if (isrp()) {
        if (scoreinputlock[0] == 0) {
            scoreinputselect++;
            if (scoreinputselect > 2) {
                scoreinputselect = 0;
            }
            scoreinputlock[0] = 1;
        }
    } else {
        scoreinputlock[0] = 0;
    }
    if (islp()) {
        if (scoreinputlock[1] == 0) {
            scoreinputselect--;
            if (scoreinputselect < 0) {
                scoreinputselect = 2;
            }
            scoreinputlock[1] = 1;
        }
    } else {
        scoreinputlock[1] = 0;
    }

    scoretextupdate();

    if (scoreinput)
        scorehighupdate();
    scoreblinktimer = scoreblinktimer + 8;

    scoredisplaybg();
    

    if (getfade() == 1 && updatefade() == 1) {
        endscorescene();
        return;
    } else {
        updatefade();
    }
}

void endscorescene() {
    writehighscore(scoreinput);
    setgameseq(0); /* to option */
    setvolume(optionvolume * 12);
    free(txscorebg);
    stopsound();
    scoreinput = 0;
    gamescore = 0;
    isloads = 0;
}