#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mikmod.h>

int isloadl = 0;
int loadtimer = 0;
GLubyte *txlogo;

void loadlogotex() {
    unsigned char *tex = loadrle("rle/logo");
    txlogo = (GLubyte *)malloc(512*512*4);
    memcpy(txlogo, tex, 512*512*4);
    free(tex);
}

void loadlogosnd() {

}

void endlogoscene() {
    free(txlogo);
    isloadl = 0;
    setgameseq(0);
}

void logoscene() {
    if(isloadl == 0) {
        loadlogotex();
        loadlogosnd();
        setentext(0, 0);
        isloadl = 1;
    }
    if (loadtimer > 30)
        setfade(1);
    if (getfade() == 1 && updatefade() == 1) {
        loadtimer = 0;
        endlogoscene();
        return;
    } else {
        updatefade();
        loadtimer++;
    }
      glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, txlogo);
  glEnable(GL_TEXTURE_2D);
  
  glNormal3d(0.0, 0.0, 1.0);
  glBegin(GL_QUADS);
  glTexCoord2d(0.0, 0.0);
  glVertex3d(-2.4, -2.4,  0.0);
  glTexCoord2d(1.0, 0.0);
  glVertex3d( 2.4, -2.4,  0.0);
  glTexCoord2d(1.0, 1.0);
  glVertex3d( 2.4,  2.4,  0.0);
  glTexCoord2d(0.0, 1.0);
  glVertex3d(-2.4,  2.4,  0.0);
  glEnd();

  glBindTexture(GL_TEXTURE_2D, 0);

  glDisable(GL_BLEND);
}